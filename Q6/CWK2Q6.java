import java.io.*;
import java.nio.file.NoSuchFileException;
import java.util.Arrays;

/**
 * @author Anonymous (do not change)
 * <p>
 * Question 6:
 * <p>
 * Implement in Java a similar algorithm to that in Q5, i.e. given a
 * block of text your algorithm should be able to redact words from
 * a given set and outputs the result to a file called â€œresult.txtâ€�.
 * However, in this implementation of the algorithm all
 * redactable words will be proper nouns (i.e. a name used for an
 * individual person, place, or organisation, spelled with an initial
 * capital letter) and your algorithm should take into account that
 * the list of redactable words might not be complete. For example,
 * given the block of text:
 * It was in July, 1805, and the speaker was the well-known Anna
 * Pavlovna Scherer, maid of honor and favorite of the Empress
 * Marya Fedorovna. With these words she greeted Prince Vasili
 * Kuragin, a man of high rank and importance, who was the first
 * to arrive at her reception. Anna Pavlovna had had a cough for
 * some days. She was, as she said, suffering from la grippe;
 * grippe being then a new word in St. Petersburg, used only by
 * the elite.
 * <p>
 * and the redactable set of words
 * Anna Pavlovna Scherer, St. Petersburg, Marya Fedorovna
 * <p>
 * the output text should be
 * It was in ****, 1805, and the speaker was the well-known ****
 * ******** *******, maid of honor and favorite of the *******
 * ***** *********. With these words she greeted ****** ******
 * *******, a man of high rank and importance, who was the first
 * to arrive at her reception. **** ******** had had a cough for
 * some days. She was, as she said, suffering from la grippe;
 * grippe being then a new word in *** **********, used only by
 * the elite.
 * <p>
 * You should test your program using the example files provided.
 */

public class CWK2Q6 {

    /**
     * Redacts words in a text file based on file containing files to be redacted
     *
     * @param textFilename        Input file to be redacted
     * @param redactWordsFilename file containing words to be redacted
     */
    public static void redactWords(String textFilename, String redactWordsFilename) {
        String line;
        String[] redactWordsArray = getRedactWordsArray(redactWordsFilename);
        if (redactWordsArray == null) System.exit(1);

        try (BufferedReader br = new BufferedReader(new FileReader(textFilename));
             PrintWriter writer = new PrintWriter("./Q6/result.txt")) {

            // read redacted words file and convert to array
            while ((line = br.readLine()) != null) {
                for (String element : redactWordsArray) {
                    // if element is in sentence, replace that word in sentence with redacted word
                    if (line.contains(element)) {
                        line = line.replaceAll(element, redactWord(element));
                    }
                }
                line = processLine(line);
                // write line to output file;
                writer.println(line);
            }
            System.out.println("Please check 'Q6/result.txt' file for the redacted text.");
        } catch (FileNotFoundException e) {
            System.err.println("File Not Found Exception: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Exception caught: " + e);
        }
    }

    /**
     * Process a line of texts for redaction
     *
     * @param line line to be processed
     * @return processed line with redactions made where necessary.
     */
    private static String processLine(String line) {
        // loop through words, and check if each word is a proper noun and redact
        String[] lineArr = line.split(" ");
        for (int i = 1; i < lineArr.length; i++) {
            String word = lineArr[i], prevWord = lineArr[i - 1];

            // if word is capitalized and is not the start of a sentence. Note that a fullstop appearing at the end of a word to be redacted should have been taken care of in the earlier loop
            if (!word.equals("I") && !prevWord.contains(".") && !prevWord.contains("?") && !prevWord.contains("!")) {
                // check if any part of the word starts with capital letter and redact starting from the capitalized letter to the end
                boolean isWordToRedactFound = false;
                StringBuilder wordToBeRedacted = new StringBuilder();

                for (int j = 0; j < word.length(); j++) {
                    char character = word.charAt(j);

                    if (isWordToRedactFound) { // this is set to true as soon as the start of a proper noun is found
                        if (!Character.isAlphabetic(character)) {
                            // end of the word to be redacted is reached. redact word without any ending non-alphabetic chars and append any non-alphabetic chars after redaction
                            String newWord = redactWord(wordToBeRedacted.toString()) + word.substring(j); //combine redacted and unredacted characters to make up the word
                            line = line.replace(word, newWord);
                            break;
                        }
                        wordToBeRedacted.append(character);
                        if (j == word.length() - 1) {
                            // end of the word to be redacted is reached. redact word and end loop
                            String newWord = redactWord(wordToBeRedacted.toString());
                            line = line.replace(word, newWord);
                            break;
                        }
                        continue;
                    }
                    if (Character.isUpperCase(character)) {
                        isWordToRedactFound = true;
                        wordToBeRedacted.append(character);
                    }
                }
            }
        }
        return line;
    }

    /**
     * Return array containing word to be redacted from file
     *
     * @param redactWordsFilename filename to fetch redact-able words from
     * @return array of words to be redacted
     */
    private static String[] getRedactWordsArray(String redactWordsFilename) {
        String[] redactWordsArray;
        StringBuilder redactText = new StringBuilder();
        String line;
        try (BufferedReader br = new BufferedReader(new FileReader(redactWordsFilename))) {
            // read redacted words file and convert to array with unique elements
            while ((line = br.readLine()) != null) {
                redactText.append(line.trim()).append(" ");
            }
            // get unique array items
            redactWordsArray = Arrays.stream(redactText.toString().trim()
                    .split(" "))
                    .distinct()
                    .toArray(String[]::new);
            System.out.println("Redactable words array: " + Arrays.toString(redactWordsArray));

            return redactWordsArray;

        } catch (NoSuchFileException e) {
            System.err.println("File Not Found Exception: " + e.getMessage());
        } catch (IOException e) {
            System.err.println("Exception caught: " + e);
        }
        return null;
    }

    public static String redactWord(String word) {
        StringBuilder redacted = new StringBuilder();
        for (int i = 0; i < word.length(); i++) {
            redacted.append("*");
        }
        return redacted.toString();
    }

    public static void main(String[] args) {
        String inputFile = "./Q6/debate.txt";
        String redactFile = "./Q6/redact.txt";
        redactWords(inputFile, redactFile);
    }
}