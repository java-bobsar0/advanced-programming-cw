import java.util.ArrayList;

/**
 * @author Anonymous (do not change)
 * <p>
 * Question 1:
 * <p>
 * Implement the Shellsort algorithm (https://en.wikipedia.org/wiki/Shellsort)
 * for an array of up to 1000 signed doubles in Java. Your solution must use
 * concrete gaps of [1, 3, 7, 15, 31, 63, 127, 255, 511]. Your solution must
 * print the (partially) sorted array after each gap on a new line in the form:
 * [a0, a1, a2, a3, ..., an]
 * Where an is the nth element in the (partially) sorted array (please note
 * the space after the commas), and each element should be formatted to 2
 * decimal places (e.g. 1.00).
 */

public class CWK2Q1 {

    /**
     * Performs shell sort on a given array
     *
     * @param array array to be sorted
     */
    public static void shell_sort(ArrayList<Double> array) {
        System.out.println("Original array\n" + array);
        int[] gaps = {1, 3, 7, 15, 31, 63, 127, 255, 511};
        int arraySize = array.size();

        // loop from end of the gaps array
        for (int gapIndex = gaps.length - 1; gapIndex >= 0; gapIndex--) {
            int gap = gaps[gapIndex];
            // no need to continue sorting if current gap is more than the size of the array.
            if (gap > arraySize) {
                continue;
            }
            performSorting(array, gap);

            System.out.println("Sorted array(formatted to 2 decimal places) after gap " + gap);
            printArray(array);
        }
    }

    /*
        Perform sorting on the array with the given gap
     */
    private static void performSorting(ArrayList<Double> array, int gap) {
        int arraySize = array.size();

        // loop from gap size and increment till arraySize. j keeps note of the current highest array index being compared against. This is mostly same as secondIndex but can be different as we'll see below
        for (int j = gap; j < arraySize; j++) {
            // firstIndex marks start of index for comparison and starts from (secondIndex - gap). It is decremented by gap in next iteration to check for a previous element which can further be compared
            for (int firstIndex = j - gap; firstIndex >= 0; firstIndex -= gap) {
                // introduce secondIndex different from j because if firstIndex becomes a previous index (through firstIndex-=gap), secondIndex will no longer be j and will still be firstIndex+gap
                int secondIndex = firstIndex + gap;
                if (array.get(secondIndex) > array.get(firstIndex)) {
                    break;
                }
                swapElements(array, firstIndex, secondIndex);
            }
        }
    }

    /*
        Swap positions of the elements in the array at the given indices
     */
    private static void swapElements(ArrayList<Double> array, int i, int j) {
        Double firstElement = array.get(i);
        Double secondElement = array.get(j);
        array.set(i, secondElement);
        array.set(j, firstElement);
    }

    /*
        Print the given array
     */
    private static void printArray(ArrayList<Double> array) {
        int arraySize = array.size();
        System.out.print("[");
        // print elements with comma and space up until the penultimate element
        for (int i = 0; i < arraySize - 1; i++) {
            System.out.printf("%.2f, ", array.get(i));
        }
        // print last element and closing bracket
        System.out.printf("%.2f]\n", array.get(arraySize - 1));
    }

    public static void main(String[] args) {
        ArrayList<Double> testList = new ArrayList<Double>();
        testList.add(3.4);
        testList.add(6.55);
        testList.add(-12.2);
        testList.add(1.73);
        testList.add(140.98);
        testList.add(-4.18);
        testList.add(52.87);
        testList.add(99.14);
        testList.add(73.202);
        testList.add(-23.6);

        shell_sort(testList);
    }
}