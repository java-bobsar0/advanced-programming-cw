import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Anonymous (do not change)
 * <p>
 * Question 2:
 * <p>
 * Implement interpolation search for a list of Strings in Java
 * using the skeleton class provided. The method should return
 * the position in the array if the string is present, or -1 if
 * it is not present.
 */

public class CWK2Q2 {

    /**
     * Performs interpolation search on a list of strings for a given string item.
     * This sorts the list in ascending order before search is conducted.
     *
     * @param array arrayList containing strings that the search is performed against.
     * @param item  item to be searched for. Casing is not considered here - for eg. if STEVE is in the list and 'Steve' or 'steve' is the searched item, search will return index of 'STEVE' in the list
     * @return item index in the sorted list or -1 if item is not present.
     */
    public static int interpolation_search(ArrayList<String> array, String item) {
        sortList(array);

        int startIndex = 0;
        int endIndex = array.size() - 1;

        // while startIndex is before or same as endIndex and item is within the range of startIndex and endIndex, derive midIndex and check if string at that midIndex is same as item
        while (startIndex <= endIndex && item.compareToIgnoreCase(array.get(startIndex)) >= 0 && item.compareToIgnoreCase(array.get(endIndex)) <= 0) {
            try {
                int midIndex = getMiddleIndex(array, item, startIndex, endIndex);
                if (item.equalsIgnoreCase(array.get(midIndex))) {
                    return midIndex;
                } else if (item.compareToIgnoreCase(array.get(midIndex)) > 0) {
                    // if item is greater than that at midIndex, move startIndex to the index of the element after the current midIndex item
                    startIndex = midIndex + 1;
                } else {
                    // if midIndex > endIndex, we dont want the next endIndex to be greater than the current one, so we decrement current endIndex
                    // otherwise, move endIndex to the index of the element before the current midIndex item
                    endIndex = midIndex > endIndex ? endIndex - 1 : midIndex - 1;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                // This exception is thrown when midIndex >= length of array. In this case, decrement endIndex since item midIndex shouldn't be outside/greater than the current endIndex
                endIndex--;
            }
        }
        return -1; // if item is not found
    }

    /*
     sort list in ascending order according to the natural ordering of its elements
     */
    private static void sortList(List<String> array) {
        Collections.sort(array);
    }

    /*
        Get middleIndex using formula below obtained from https://www.tutorialspoint.com/data_structures_algorithms/interpolation_search_algorithm.htm
        Mid = Lo + ((Hi - Lo) / (Array[Hi] - Array[Lo])) * (item - Array[Lo])
        This formula has been adapted to work for strings where subtraction is taken as distance between the 2 strings and is derived from getDifference method
     */
    private static int getMiddleIndex(List<String> array, String item, int low, int high) {
        String highItem = array.get(high);
        String lowItem = array.get(low);
        double x = (double) (high - low) / (getDifference(lowItem, highItem));
        return low + (int) (x * getDifference(lowItem, item));
    }

    /*
        Get difference/subtraction between two strings
     */
    private static int getDifference(String x, String y) {
        // find longest string so the length can be used for iteration
        int minLength = Math.min(x.length(), y.length());

        // compare each index of string and return absolute difference when a non-matching character is encountered in a given index
        for (int i = 0; i < minLength; i++) {
            // Get int values of the first characters and perform subtraction
            int xChar = x.toUpperCase().charAt(i);
            int yChar = y.toUpperCase().charAt(i);
            if (xChar != yChar) {
                return Math.abs(yChar - xChar);
            }
        }
        // return 0 if after iteration, no dissimilar character is found in both strings.
        return 0;
    }

    public static void main(String[] args) {
        ArrayList<String> testList = new ArrayList<String>();
        testList.add("Hello");
        testList.add("World");
        testList.add("How");
        testList.add("Are");
        testList.add("You");

        int result = interpolation_search(testList, "How");
        System.out.println("Result = " + result);
    }
}