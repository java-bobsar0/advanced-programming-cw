/**
 * @author Anonymous (do not change)
 * <p>
 * Question 8:
 * <p>
 * You are given the following information, but you may prefer
 * to do some research for yourself.
 * •	1 Jan 1900 was a Monday.
 * •	Thirty days has September, April, June and November. All the rest
 * have thirty-one, saving February alone, which has twenty-eight, rain
 * or shine. And on leap years, twenty-nine.
 * •	A leap year occurs on any year evenly divisible by 4, but not on a
 * century unless it is divisible by 400.
 * <p>
 * How many Tuesdays fell on the first of the month during the twentieth
 * century (1 Jan 1901 to 31 Dec 2000)?
 * <p>
 * Note, this problem is inspired by Project Euler so, as stated in the
 * rules of Project Euler, your solution should return an answer under
 * 60 seconds.
 */

public class CWK2Q8 {
    /**
     * Calculate count of Tuesdays that begin each month between 1901 - 2000
     * Days are represented as integers from 1-7 with Monday as 1, Tuesday as 2 etc
     * Months are represented as integers from 1-12 with January as 1, February as 2 etc
     *
     * @return number of Tuesdays that begin each month from 1901 to 2000
     */
    public static int howManyTuesdays() {
        int firstDayJan1900 = 1;
        int numberOfDaysIn1900 = isLeapYear(1900) ? 366 : 365;

        // day + 7 will give us the same day: Monday + 7 = Monday. numberOfDays mod 7 is calculated to give days to advance by.
        int firstDayPrevMonth = firstDayJan1900 + ((numberOfDaysIn1900 - 31) % 7);//31 is subtracted from numberOfDays to get number of days between Jan1 and Nov31

        int totalTuesdays = 0;
        for (int year = 1901; year <= 2000; year++) {
            for (int i = 1; i <= 12; i++) { //loop through months
                // start day of current month = (no of days in prev month + start day of previous month) % 7
                int startOfMonthInt = (getDaysInMonth(i - 1, year) + firstDayPrevMonth) % 7;
                firstDayPrevMonth = startOfMonthInt == 0 ? 7 : startOfMonthInt; // 0th day is same as 7th day
                if (startOfMonthInt == 2) totalTuesdays++;
            }
        }
        return totalTuesdays;
    }

    /*
        Get total number of days for a particular month in a year
        Note: Months are represented with numbers from 1-12 with January as 1 and month 12 is same as month 0
     */
    private static int getDaysInMonth(int month, int year) {
        switch (month) {
            case 2:
                return isLeapYear(year) ? 29 : 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 31;
        }
    }

    private static boolean isLeapYear(int year) {
        // return true if year is divisible by 4 and not a century or if year is a century divisible by 400
        return year % 4 == 0 && (year % 100 != 0 || year % 400 == 0);
    }

    public static void main(String[] args) {
        int result = CWK2Q8.howManyTuesdays();
        System.out.println("Number of Tuesdays = " + result);
    }
}